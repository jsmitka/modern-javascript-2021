const values = [1, 2, 3, 4, 5, 6];

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

console.log(
    values.filter(value => value % 2 === 0)
);

console.log(
    values.map(value => value * 2)
);

values.forEach(value => {
    console.log(`handler: ${value}`);
});

// Chaining:
console.log(
    values
        .filter(value => value % 2 === 0)
        .map((value, index) => value * index)
);
const objectValues = {
    'a': 1,
    'b': 2,
    'c': 3,
    'd': 4,
};

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object

console.log(
    Object.keys(objectValues)
);

console.log(
    Object.values(objectValues)
);

console.log(
    Object.entries(objectValues)
);

console.log(
    Object.assign({'d': 42, 'e': 5}, objectValues)
);

console.log(
    Object.fromEntries([
        ['a', 1],
        ['b', 2],
    ])
);

/**
 * @returns <Promise>
 */
function sleepPromise(sleepMillis, value) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (value) {
                resolve(value);
            } else {
                reject('No value given!');
            }
        }, sleepMillis);
    });
}

// Handler chaining
console.log('Starting');
sleepPromise(500, 'original')
    .then(value => {
        console.log(value);
        return `${value} A`;
    })
    .then(value => {
        console.log(value);
        return 'replaced';
    })
    .then(value => {
        console.log(value);
    })
    .then(value => {
        console.log(value);
    });

// Rejections
sleepPromise(1000, null)
    .then(value => {
        console.log('Successful');
    })
    .catch(error => {
        console.log(`Error: ${error}`);
    })

// Errors.
sleepPromise(1500, 'error')
    .then(value => {
        throw new Error('Thrown an error!');
    })
    .catch(error => {
        console.log(`Catched: ${error}`);
    })
    .finally(value => {
        console.log('Finally!');
        console.log(value);
    });

// Return a promise
sleepPromise(2000, 'value')
    .then(value => {
        console.log('First resolve')
        return sleepPromise(1000, 'new value');
    })
    .then(value => {
        console.log(value);
    });

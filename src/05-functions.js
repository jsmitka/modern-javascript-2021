class Timer {
    constructor(delay) {
        this.remainingTime = delay;
        this.interval = null;
    }

    start() {
        console.log(this.remainingTime);
        this.interval = setInterval(() => {
            if (this.remainingTime > 1) {
                this.remainingTime--;
                console.log(this.remainingTime);
            } else {
                console.log('Done!');
                clearInterval(this.interval);
            }
        }, 1000);
    }
}

const timer = new Timer(5);
timer.start();

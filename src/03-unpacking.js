const values = [1, 2, 3];

const [a, b, c] = values;
// equals to
{
    const a = values[0];
    const b = values[1];
    const c = values[2];
}

console.log('a = %o', a);
console.log('b = %o', b);
console.log('c = %o', c);


const obj = {'x': 12, 'y': 24, 'z': 36};
const {x, y, z} = obj;
console.log('x = %o', x);
console.log('y = %o', y);
console.log('z = %o', z);

const {x: x2, y: y2, z: z2} = obj;
console.log('x2 = %o', x2);
console.log('y2 = %o', y2);
console.log('z2 = %o', z2);


function getTotalPrice({price, quantity}) {
    console.log('Total Price: %d', quantity * price)
}
// Roughly equals to
function getTotalPriceOld(obj) {
    let quantity = obj.quantity;
    let price = obj.price;
    console.log('Total Price: %d', quantity * price)
}

getTotalPrice({quantity: 2, price: 10});


const answer = '42';
const universe = '42';

const obj = {answer, universe};
// equals to
const obj = {'answer': answer, 'universe': universe};

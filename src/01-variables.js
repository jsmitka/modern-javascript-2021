main();

function main() {
    var a = 10;

    if (true) {
        let b = 42;
        const c = {'a': 1};

        var d = 15;

        console.log('a = %o', a);
        console.log('b = %o', b);
    }

    console.log('b = %o', b);

    console.log('d = %o', d);
    // console.log(c);

    function value () {
        console.log('a = %o', a);
    }
    a++;
    value();
}

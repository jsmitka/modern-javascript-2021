class Order {
    static NEXT_ID = 1;

    constructor(itemsPrice, deliveryPrice) {
        this._id = Order.getNextId();
        this._itemsPrice = itemsPrice;
        this._deliveryPrice = deliveryPrice;
    }

    static getNextId() {
        return this.NEXT_ID++;
    }

    get id() {
        return this._id;
    }

    get itemsPrice() {
        return this._itemsPrice;
    }

    get deliveryPrice() {
        return this._deliveryPrice;
    }

    set deliveryPrice(value) {
        console.warn('Warning! Updating delivery price after instance creation.');
        this._deliveryPrice = value;
    }

    computeTotalPrice() {
        return this._itemsPrice + this._deliveryPrice;
    }
}

// Python: class Order2(Order):
class Order2 extends Order {

}


const order1 = new Order(150, 25);
const order2 = new Order(6000, 0);

order1.deliveryPrice = 150;
// You can access "private" variables
console.log(order1._deliveryPrice);

console.log('[Order ID %d] %d', order1.id, order1.computeTotalPrice());
console.log('[Order ID %d] %d', order2.id, order2.computeTotalPrice());

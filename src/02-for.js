
const values = [10, 20, 30];

// Python: for i in range(values.length):
for (let i = 0; i < values.length; i++) {
    console.log('  for: values[%d] = %o', i, values[i]);
}

for (const i in values) {
    console.log('- for...in: values[%d] = %o', i, values[i]);
}

for (const value of values) {
    console.log('  for...of: %o', value);
}


const obj = {
    'a': 1,
    'b': 2,
    'c': 3,
};

for (const prop in obj) {
    console.log('- for...in with obj: obj[%s] = %o', prop, obj[prop]);
}
